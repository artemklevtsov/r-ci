# GitLab CI template for the R package check

## Structure

* `check-linux.yml`: check R package on Linux
* `check-win.yml`: check R package on Windows
* `code-coverage.yml`: check code coverage with `covr` package
* `code-lint.yml`: lint code with `lintr` package
* `pages-pkgdown.yml`: build pkgdown site
* `ci.yml`: place all together

## Usage

Add the following code to `.gitlab-ci.yml`:

```yml
include:
  - project: artemklevtsov/r-ci
    ref: master
    file: /ci.yml

check:linux:release:
  extends: .check:linux:release

check:linux:oldrel:
  extends: .check:linux:oldrel

check:linux:devel:
  extends: .check:linux:devel

check:linux:patched:
  extends: .check:linux:patched

check:linux:devel-san:
  extends: .check:linux:devel-san

check:linux:devel-ubsan-clang:
  extends: .check:linux:devel-ubsan-clang

check:windows:release:
  extends: .check:linux:release

check:windows:oldrel:
  extends: .check:windows:oldrel

check:windows:devel:
  extends: .check:linux:devel

check:windows:patched:
  extends: .check:linux:patched

code:coverage:
  extends: .code:coverage

code:lint:
  extends: .code:lint

pages:
  extends: .pages:pkgdown
```

You can use only needed files only:

```yml
include:
  - project: artemklevtsov/r-ci
    ref: master
    file: /templates/check-linux.yml
  - project: artemklevtsov/r-ci
    ref: master
    file: /templates/check-win.yml
```
